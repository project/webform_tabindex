Description
-----------
The webform tabindex allows web developers to customize the tab/navigation
order of web content on which HTML objects(e.g. input, select, fieldset etc )
that support tabindex attribute.

By default webform set the tab/navigation order in which element are rendering
on the page. By using webform tabindex module developers can easily set the
tab/navigation order for focusable elements (typically form controls).

Now this module also support for skip the tabindex from specific form element. 

Installation
------------
1. Copy the entire webform_tabindex directory
   the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Create a webform node at node/add/webform.
4. Access the links (node/node_id/webform/tabindex)
   to manage tab/navigation order of webform elements.

DEPENDENCIES:
-------------
webform

Support
-------
Please use the issue queue for filing bugs with this module at
https://www.drupal.org/project/issues/webform_tabindex?status=All&categories=All

AUTHOR:
-------
Devendra Yadav <dev.firoza@gmail.com>
